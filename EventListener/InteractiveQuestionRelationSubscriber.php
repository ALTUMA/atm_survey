<?php

namespace ATM\SurveyBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;

class InteractiveQuestionRelationSubscriber implements EventSubscriber
{
    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function getSubscribedEvents()
    {
        return array(
            Events::loadClassMetadata
        );
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $metadata = $eventArgs->getClassMetadata();

        if ($metadata->getName() != 'ATM\SurveyBundle\Entity\InteractiveQuestion') {
            return;
        }

        $metadata->mapManyToOne(array(
            'targetEntity' => $this->config['user'],
            'fieldName' => 'author',
            'joinColumns' => array(
                array(
                    'name' => 'author_id',
                    'referencedColumnName' => 'id'
                )
            )
        ));

        $metadata->mapManyToOne(array(
            'targetEntity' => $this->config['user'],
            'fieldName' => 'model',
            'joinColumns' => array(
                array(
                    'name' => 'model_id',
                    'referencedColumnName' => 'id'
                )
            )
        ));
    }
}