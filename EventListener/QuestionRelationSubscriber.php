<?php

namespace ATM\SurveyBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;

class QuestionRelationSubscriber implements EventSubscriber
{
    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function getSubscribedEvents()
    {
        return array(
            Events::loadClassMetadata
        );
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $metadata = $eventArgs->getClassMetadata();

        if ($metadata->getName() != 'ATM\SurveyBundle\Entity\Question') {
            return;
        }

        $metadata->mapManyToOne(array(
            'targetEntity' => $this->config['user'],
            'fieldName' => 'askedBy',
            'joinColumns' => array(
                array(
                    'name' => 'askedBy_id',
                    'referencedColumnName' => 'id'
                )
            )
        ));
    }
}