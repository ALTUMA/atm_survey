<?php

namespace ATM\SurveyBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class InteractiveQuestionAnswered extends Event{

    const NAME = 'atm_interactive_question_answered.event';

    private $interactiveQuestion;


    public function __construct($interactiveQuestion)
    {
        $this->interactiveQuestion = $interactiveQuestion;
    }

    public function getInteractiveQuestion()
    {
        return $this->interactiveQuestion;
    }
}