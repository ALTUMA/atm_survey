<?php

namespace ATM\SurveyBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class InteractiveQuestionAnswerApproved extends Event{

    const NAME = 'atm_interactive_question_answer_approved.event';

    private $interactiveQuestion;


    public function __construct($interactiveQuestion)
    {
        $this->interactiveQuestion = $interactiveQuestion;
    }

    public function getInteractiveQuestion()
    {
        return $this->interactiveQuestion;
    }
}