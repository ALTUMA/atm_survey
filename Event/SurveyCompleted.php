<?php

namespace ATM\SurveyBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class SurveyCompleted extends Event{

    const NAME = 'atm_survey_completed.event';

    private $survey;
    private $answers;
    private $user;

    public function __construct($survey,$answers, $user)
    {
        $this->survey = $survey;
        $this->answers = $answers;
        $this->user = $user;
    }

    public function getSurvey()
    {
        return $this->survey;
    }

    public function setSurvey($survey)
    {
        $this->survey = $survey;
    }

    public function getAnswers()
    {
        return $this->answers;
    }

    public function setAnswers($answers)
    {
        $this->answers = $answers;
    }

    public function getUser()
    {
        return $this->user;
    }
    public function setUser($user)
    {
        $this->user = $user;
    }
}