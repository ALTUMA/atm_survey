<?php

namespace ATM\SurveyBundle\Helpers;

use \Imagick;

class Image{
    public static function removeExif($source_file, $destination_file = false)
    {
        $destination_file = $destination_file ? $destination_file : $source_file;
        $exif = new Imagick($destination_file);
        $profiles = $exif->getImageProfiles("icc", true);
        $exif->stripImage();
        if(!empty($profiles))
        {
            $exif->profileImage("icc", $profiles['icc']);
        }
        $exif->writeImage($destination_file);
    }

    public static function correctImageOrientation($source_file, $destination_file = false)
    {
        $destination_file = $destination_file ? $destination_file : $source_file;
        if(function_exists('exif_read_data'))
        {
            $exif = exif_read_data($source_file);
            if($exif && isset($exif['Orientation']))
            {
                $orientation = $exif['Orientation'];
                if($orientation != 1)
                {
                    $img = imagecreatefromjpeg($source_file);
                    $deg = 0;
                    switch ($orientation) {
                        case 3:
                            $deg = 180;
                            break;
                        case 6:
                            $deg = 270;
                            break;
                        case 8:
                            $deg = 90;
                            break;
                    }
                    if ($deg)
                    {
                        $img = imagerotate($img, $deg, 0);
                    }
                    // then rewrite the rotated image back to the disk as $destination_file
                    imagejpeg($img, $destination_file, 95);
                } // if there is some rotation necessary
            } // if have the exif orientation info
        } // if function exists
    }

    public static function getFirstFrameImageFromGif($gif_file,$destination_folder){

        $imagick = new Imagick($gif_file);

        $imagick = $imagick->coalesceImages();

        foreach ($imagick as $frame) {
            $filename = md5(uniqid()).'.jpg';
            $frame->writeImage($destination_folder.$filename);
            return $filename;
        }

        return null;
    }

    public static function resize_crop_image($max_width, $max_height, $source_file, $dst_dir, $quality = 80)
    {
        $imgsize = getimagesize($source_file);
        $width = $imgsize[0];
        $height = $imgsize[1];
        $mime = $imgsize['mime'];

        switch($mime)
        {
            case 'image/gif':
                $image_create = "imagecreatefromgif";
                $image = "imagegif";
                break;
            case 'image/png':
                $image_create = "imagecreatefrompng";
                $image = "imagepng";
                $quality = 7;
                break;
            case 'image/jpeg':
                $image_create = "imagecreatefromjpeg";
                $image = "imagejpeg";
                $quality = 99;
                break;
            default:
                return false;
                break;
        }

        list($width, $height) = getimagesize($source_file);
        if($max_height == 'auto')
        {
            $max_height = $max_width * $height / $width;
        } elseif($max_width == 'auto') {
            $max_width = $max_height * $width / $height;
        }

        $dst_img = imagecreatetruecolor($max_width, $max_height);
        $src_img = $image_create($source_file);

        $width_new = $height * $max_width / $max_height;
        $height_new = $width * $max_height / $max_width;
        //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
        if($width_new > $width)
        {
            //cut point by height
            $h_point = (($height - $height_new) / 2);
            //copy image
            imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
        } else {
            //cut point by width
            $w_point = (($width - $width_new) / 2);
            imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
        }

        $image($dst_img, $dst_dir, $quality);

        if($dst_img)imagedestroy($dst_img);
        if($src_img)imagedestroy($src_img);
        return true;
    }
}