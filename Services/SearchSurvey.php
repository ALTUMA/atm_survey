<?php

namespace ATM\SurveyBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;

class SearchSurvey{
    private $em;
    private $paginator;

    public function __construct(EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    public function search($options)
    {
        $defaultOptions = array(
            'answered' => true,
            'date' => null,
            'exclude_ids' => null,
            'only_init_date' => null,
            'ids' => null,
            'pagination' => null,
            'order_by_field' => 'init_date',
            'order_by_direction' => 'DESC',
            'max_results' => null,
            'page' => 1
        );

        $options = array_merge($defaultOptions, $options);

        $qbIds = $this->em->createQueryBuilder();
        $qbIds
            ->select('partial s.{id}')
            ->from('ATMSurveyBundle:Survey','s');

        if($options['answered']){
            $qbIds
                ->join('s.answers','a','WITH',$qbIds->expr()->isNotNull('a.answers'))
                ->join('a.user','u','WITH',$qbIds->expr()->eq('u.enabled',1))
            ;
        }

        if(!is_null($options['only_init_date'])){
            $qbIds->andWhere($qbIds->expr()->lte('s.init_date',$qbIds->expr()->literal($options['date'].' 23:59:59')));
        }elseif(!is_null($options['date'])){
            $qbIds->andWhere(
                $qbIds->expr()->andX(
                    $qbIds->expr()->lte('s.init_date',$qbIds->expr()->literal($options['date'].' 00:00:00')),
                    $qbIds->expr()->gte('s.end_date',$qbIds->expr()->literal($options['date'].' 23:59:59'))
                )
            );
        }

        if(!is_null($options['exclude_ids'])){
            $qbIds->andWhere($qbIds->expr()->notIn('s.id',$options['exclude_ids']));
        }

        if(!is_null($options['ids'])){
            $qbIds->andWhere($qbIds->expr()->in('s.id',$options['ids']));
        }

        $qbIds->orderBy('s.'.$options['order_by_field'],$options['order_by_direction']);
        $pagination = null;
        
        if(!is_null($options['pagination'])){
            $arrIds = array_map(function($p){
                return $p['id'];
            },$qbIds->getQuery()->getArrayResult());

            $pagination = $this->paginator->paginate(
                $arrIds,
                is_null($options['page']) ? 1 : $options['page'],
                is_null($options['max_results']) ? 10 : $options['max_results']
            );

            $ids = $pagination->getItems();
        }else {
            $query = $qbIds->getQuery();
            if(!is_null($options['max_results'])){
                $query->setMaxResults($options['max_results']);
            }

            $ids = array_map(function ($p) {
                return $p['id'];
            }, $query->getArrayResult());
        }

        $surveys = array();
        if(count($ids) > 0){
            $qb = $this->em->createQueryBuilder();
            $qb
                ->select('s')
                ->addSelect('a')
                ->addSelect('u')
                ->from('ATMSurveyBundle:Survey','s')
                ->leftJoin('s.answers','a')
                ->leftJoin('a.user','u')
                ->where($qb->expr()->in('s.id',$ids))
                ->orderBy('s.'.$options['order_by_field'],$options['order_by_direction']);

            $surveys = $qb->getQuery()->getArrayResult();
        }

        return array(
            'results' => $surveys,
            'pagination' => $pagination
        );
    }

}