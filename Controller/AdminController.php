<?php

namespace ATM\SurveyBundle\Controller;

use ATM\SurveyBundle\Entity\AllowQuestions;
use ATM\SurveyBundle\Event\InteractiveQuestionAnswerApproved;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ATM\SurveyBundle\Entity\Survey;
use ATM\SurveyBundle\Form\SurveyType;
use ATM\SurveyBundle\Entity\Question;
use ATM\SurveyBundle\Entity\Choice;
use ATM\SurveyBundle\Event\InteractiveQuestionReviewed;
use \DateTime;

class AdminController extends Controller{

    /**
     * @Route("/survey/list", name="atm_survey_list")
     */
    public function surveyListAction(){
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb
            ->select('partial s.{id,name,creation_date, init_date, end_date}')
            ->addSelect('partial q.{id}')
            ->from('ATMSurveyBundle:Survey','s')
            ->leftJoin('s.questions','q')
            ->orderBy('s.init_date','DESC');

        $surveys = $qb->getQuery()->getArrayResult();

        return $this->render('ATMSurveyBundle:Admin:index.html.twig',array(
            'surveys' => $surveys
        ));
    }

    /**
     * @Route("/survey/delete/{surveyId}", name="atm_survey_delete")
     */
    public function surveyDeleteAction($surveyId){
        $em = $this->getDoctrine()->getManager();

        $survey = $em->getRepository('ATMSurveyBundle:Survey')->findOneById($surveyId);

        $answers = $em->getRepository('ATMSurveyBundle:Answer')->findBy(array('survey'=>$surveyId));
        foreach($answers as $answer){
            $em->remove($answer);
        }

        $em->remove($survey);
        $em->flush();

        return  $this->redirect($this->get('router')->generate('atm_survey_list'));
    }

    /**
     * @Route("/survey/create", name="atm_survey_create")
     */
    public function createSurveyAction(){
        $em = $this->getDoctrine()->getManager();

        $survey = new Survey();
        $form = $this->createForm(SurveyType::class,$survey);

        $request = $this->get('request_stack')->getCurrentRequest();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($survey);
            $em->flush();

            return $this->redirect($this->get('router')->generate('atm_survey_create_questions',array('surveyId'=>$survey->getId())));
        }

        return $this->render('ATMSurveyBundle:Admin:Survey/create.html.twig',array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/survey/set/questions/{surveyId}", name="atm_survey_create_questions")
     */
    public function createQuestionsAction($surveyId){
        $em = $this->getDoctrine()->getManager();

        $survey = $em->getRepository('ATMSurveyBundle:Survey')->findOneById($surveyId);

        $request = $this->get('request_stack')->getCurrentRequest();
        if($request->getMethod() == 'POST'){
            $questionsJson = $request->get('questions');
            $arrQuestions = json_decode($questionsJson,true);

            $config = $this->getParameter('atm_survey_config');
            $mediaDir = $this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'];

            if(!is_dir($mediaDir)){
                mkdir($mediaDir,0777);
            }

            $headerImage = $request->files->get('flHeaderImage');
            if(!is_null($headerImage)){
                $extension = $headerImage->guessExtension();
                $filename = md5(uniqid()).'.'.$extension;
                $headerImage->move($mediaDir,$filename);

                $survey->setHeaderImage($config['media_folder'].'/'.$filename);
                $em->persist($survey);
            }

            $headerImageMobile = $request->files->get('flHeaderImageMobile');
            if(!is_null($headerImageMobile)){
                $filenameMobile = md5(uniqid()).'.'.$headerImageMobile->guessExtension();
                $headerImageMobile->move($mediaDir,$filenameMobile);
                $survey->setHeaderImageMobile($config['media_folder'].'/'.$filenameMobile);
                $em->persist($survey);
            }

            $headerImageMiddle = $request->files->get('flHeaderImageMiddle');
            if(!is_null($headerImageMiddle)){
                $filenameMiddle = md5(uniqid()).'.'.$headerImageMiddle->guessExtension();
                $headerImageMiddle->move($mediaDir,$filenameMiddle);
                $survey->setHeaderImageMiddle($config['media_folder'].'/'.$filenameMiddle);
                $em->persist($survey);
            }

            $profileImage = $request->files->get('flProfileImage');
            if(!is_null($profileImage)){
                $extension = $profileImage->guessExtension();
                $filename = md5(uniqid()).'.'.$extension;
                $profileImage->move($mediaDir,$filename);

                $survey->setProfileImage($config['media_folder'].'/'.$filename);
                $em->persist($survey);
            }

            foreach($arrQuestions as $arrQuestion){
                $question = new Question();
                $question->setQuestion($arrQuestion['text']);
                $question->setType($arrQuestion['type']);

                if(isset($arrQuestion['choices'])){
                    foreach($arrQuestion['choices'] as $arrChoice){
                        $choice = new Choice();
                        $choice->setChoice($arrChoice);
                        $choice->setQuestion($question);
                        $em->persist($choice);
                        $question->addChoice($choice);
                    }
                }
                $question->setSurvey($survey);
                $em->persist($survey);
                $em->persist($question);
            }

            $em->flush();
            return $this->redirect($this->get('router')->generate('atm_survey_list'));
        }


        return $this->render('ATMSurveyBundle:Admin:Survey/create_questions.html.twig',array(
            'survey' => $survey
        ));
    }

    /**
     * @Route("/survey/edit/questions/{surveyId}", name="atm_survey_edit_questions")
     */
    public function editQuestionsAction($surveyId){
        $em = $this->getDoctrine()->getManager();
        $survey = $em->getRepository('ATMSurveyBundle:Survey')->findOneById($surveyId);

        $request = $this->get('request_stack')->getCurrentRequest();
        if($request->getMethod() == 'POST'){
            $questionsJson = $request->get('questions');
            $arrQuestions = json_decode($questionsJson,true);

            $config = $this->getParameter('atm_survey_config');
            $web_folder = $this->get('kernel')->getRootDir().'/../web/';
            $mediaDir = $web_folder.$config['media_folder'];

            if(!is_dir($mediaDir)){
                mkdir($mediaDir,0777);
            }

            $headerImage = $request->files->get('flHeaderImage');
            if(!is_null($headerImage)){
                if(!is_null($survey->getHeaderImage()) && is_file($web_folder.$survey->getHeaderImage())){
                    unlink($web_folder.$survey->getHeaderImage());
                }

                $extension = $headerImage->guessExtension();
                $filename = md5(uniqid()).'.'.$extension;
                $headerImage->move($mediaDir,$filename);

                $survey->setHeaderImage($config['media_folder'].'/'.$filename);

                $em->persist($survey);
            }

            $headerImageMobile = $request->files->get('flHeaderImageMobile');
            if(!is_null($headerImageMobile)){
                if(!is_null($survey->getHeaderImageMobile()) && is_file($web_folder.$survey->getHeaderImageMobile())){
                    unlink($web_folder.$survey->getHeaderImageMobile());
                }

                $filenameMobile = md5(uniqid()).'.'.$headerImageMobile->guessExtension();
                $headerImageMobile->move($mediaDir,$filenameMobile);
                $survey->setHeaderImageMobile($config['media_folder'].'/'.$filenameMobile);
                $em->persist($survey);
            }

            $headerImageMiddle = $request->files->get('flHeaderImageMiddle');
            if(!is_null($headerImageMiddle)){
                if(!is_null($survey->getHeaderImageMiddle()) && is_file($web_folder.$survey->getHeaderImageMiddle())){
                    unlink($web_folder.$survey->getHeaderImageMiddle());
                }

                $filenameMiddle = md5(uniqid()).'.'.$headerImageMiddle->guessExtension();
                $headerImageMiddle->move($mediaDir,$filenameMiddle);
                $survey->setHeaderImageMiddle($config['media_folder'].'/'.$filenameMiddle);
                $em->persist($survey);
            }

            $profileImage = $request->files->get('flProfileImage');
            if(!is_null($profileImage)){
                if(!is_null($survey->getProfileImage()) && is_file($web_folder.$survey->getProfileImage())){
                    unlink($web_folder.$survey->getProfileImage());
                }

                $extension = $profileImage->guessExtension();
                $filename = md5(uniqid()).'.'.$extension;
                $profileImage->move($mediaDir,$filename);

                $survey->setProfileImage($config['media_folder'].'/'.$filename);
                $em->persist($survey);
            }

            $initDate = $request->get('initDate');
            if(!is_null($initDate)){
                $initDate = DateTime::createFromFormat('d-m-Y',$initDate);
                $survey->setInitDate($initDate);
                $em->persist($survey);
            }

            $endDate = $request->get('endDate');
            if(!is_null($endDate)){
                $endDate = DateTime::createFromFormat('d-m-Y',$endDate);
                $survey->setEndDate($endDate);
                $em->persist($survey);
            }

            foreach($arrQuestions as $arrQuestion){
                $text = $arrQuestion['text'];
                $type = $arrQuestion['type'];

                if(isset($arrQuestion['id'])){//MODIFING EXISTING QUESTION
                    $questionId = $arrQuestion['id'];
                    $question = $em->getRepository('ATMSurveyBundle:Question')->findOneById($questionId );

                    if($question->getQuestion() != $text){
                        $question->setQuestion($text);
                    }

                    if($question->getType() != $type){
                        $question->setType($type);
                    }

                    if($type == 'text' && count($question->getChoices()) > 0){
                        foreach($question->getChoices() as $choice){
                            $em->remove($choice);
                        }
                    }else if(isset($arrQuestion['choices'])){
                        $existingChoicesIds = array();
                        foreach($arrQuestion['choices'] as $arrChoice){
                            if(!isset($arrChoice['id'])){
                                $choice = new Choice();
                                $choice->setChoice($arrChoice['text']);
                                $choice->setQuestion($question);
                                $em->persist($choice);
                                $question->addChoice($choice);
                                $em->persist($question);
                            }else{
                                $existingChoicesIds[] = $arrChoice['id'];
                            }
                        }

                        foreach($question->getChoices() as $choice){
                            if(!in_array($choice->getId(),$existingChoicesIds)){
                                $em->remove($choice);
                            }
                        }
                    }
                }else{//NEW QUESTION
                    $question = new Question();
                    $question->setQuestion($text);
                    $question->setType($type);

                    if(isset($arrQuestion['choices'])){
                        foreach($arrQuestion['choices'] as $arrChoice){
                            $choice = new Choice();
                            $choice->setChoice($arrChoice['text']);
                            $choice->setQuestion($question);
                            $em->persist($choice);
                            $question->addChoice($choice);
                        }
                    }
                    $question->setSurvey($survey);
                    $em->persist($survey);
                    $em->persist($question);
                }
            }
            $em->flush();
            return $this->redirect($this->get('router')->generate('atm_survey_list'));
        }

        return $this->render('ATMSurveyBundle:Admin:Survey/edit_questions.html.twig',array(
            'survey' => $survey
        ));
    }

    /**
     * @Route("/survey/delete/question/{questionId}", name="atm_survey_delete_question", options={"expose"=true})
     */
    public function deleteQuestionAction($questionId){
        $em = $this->getDoctrine()->getManager();

        $question = $em->getRepository('ATMSurveyBundle:Question')->findOneById($questionId);
        $em->remove($question);
        $em->flush();

        return new Response('ok');
    }

    /**
     * @Route("/survey/delete/choice/{choiceId}", name="atm_survey_delete_choice", options={"expose"=true})
     */
    public function deleteChoiceAction($choiceId){
        $em = $this->getDoctrine()->getManager();

        $choice = $em->getRepository('ATMSurveyBundle:Choice')->findOneById($choiceId);
        $em->remove($choice);
        $em->flush();

        return new Response('ok');
    }

    /**
     * @Route("/survey/see/{surveyId}", name="atm_survey_see_form_admin")
     */
    public function surveySeeFormAction($surveyId){
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb
            ->select('partial s.{id,name,creation_date,header_image, profile_image}')
            ->addSelect('partial q.{id,question,type}')
            ->addSelect('partial c.{id,choice}')
            ->from('ATMSurveyBundle:Survey','s')
            ->leftJoin('s.questions','q')
            ->leftJoin('q.choices','c')
            ->where($qb->expr()->eq('s.id',$surveyId))
            ->orderBy('q.id','ASC')
        ;

        $survey = $qb->getQuery()->getArrayResult();

        return $this->render('ATMSurveyBundle:Admin:Survey/seeForm.html.twig',array(
            'survey' => $survey[0]
        ));
    }

    /**
     * @Route("/survey/see/answers/{surveyId}", name="atm_survey_see_answers")
     */
    public function seeSurveyAnswersAction($surveyId){
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb
            ->select('partial a.{id,answers_json,answers,creation_date}')
            ->addSelect('partial u.{id,username}')
            ->from('ATMSurveyBundle:Answer','a')
            ->join('a.survey','s','WITH',$qb->expr()->eq('s.id',$surveyId))
            ->join('a.user','u')
            ->orderBy('a.id','ASC');

        $answers = $qb->getQuery()->getArrayResult();

        return $this->render('ATMSurveyBundle:Admin:Answer/list.html.twig',array(
            'answers' => $answers,
            'surveyId' => $surveyId
        ));
    }

    /**
     * @Route("/survey/edit/answer/{answerId}", name="atm_survey_edit_answer")
     */
    public function editAnswerAction($answerId){
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb
            ->select('answer')
            ->addSelect('survey')
            ->addSelect('user')
            ->addSelect('question')
            ->addSelect('askedBy')
            ->addSelect('choice')
            ->from('ATMSurveyBundle:Answer','answer')
            ->join('answer.survey','survey')
            ->join('survey.questions','question')
            ->leftJoin('question.askedBy','askedBy')
            ->leftJoin('question.choices','choice')
            ->join('answer.user','user')
            ->where($qb->expr()->eq('answer.id',$answerId))
            ->orderBy('question.id','ASC')
        ;

        $answer = $qb->getQuery()->getResult()[0];

        $request = $this->get('request_stack')->getCurrentRequest();
        if($request->getMethod() == 'POST'){
            $formData = $request->request->all();
            $survey = $answer->getSurvey();
            $textAnswers = array();
            foreach($survey->getQuestions() as $question){
                $questionAnswer = $this->getQuestionAnswer($question,$formData['answers']);
                $arrQuestion = array();
                $arrQuestion['question'] = $question->getQuestion();
                $arrQuestion['answer'] = $questionAnswer;
                $askedBy = $question->getAskedBy();
                if($askedBy){
                    $arrQuestion['asked_by_username'] = $askedBy->getTitle() ? $askedBy->getTitle() : $askedBy->getUsername();
                    $arrQuestion['asked_by_username_canonical'] = $askedBy->getUsernameCanonical();
                    $arrQuestion['asked_by_avatar'] = $askedBy->getAvatar() ? $askedBy->getAvatar() : null;
                }
                $textAnswers[] = $arrQuestion;
            }

            $answer->setAnswersJson(json_encode($formData));
            $answer->setAnswers(json_encode($textAnswers));
            $em->persist($answer);
            $em->flush();

            return $this->redirect($this->get('router')->generate('atm_survey_see_answers',array('surveyId'=>$survey->getId())));
        }

        return $this->render('ATMSurveyBundle:Admin:Answer/edit.html.twig',array(
            'responses' => $answer->getAnswersJson(),
            'answer' => $answer,
            'survey' => $answer->getSurvey(),
            'user' => $answer->getUser()
        ));
    }

    private function getQuestionAnswer($question,$answers){
        foreach($answers as $answer){
            if($question->getId() == $answer['id']){
                if($question->getType() == 'text'){
                    return $answer['text'];
                }else{
                    $choices = array();
                    unset($answer['id']);
                    foreach($question->getChoices() as $choice){
                        if(in_array($choice->getId(),$answer)){
                            $choices[] = $choice->getChoice();
                        }
                    }
                    return $choices;
                }
            }
        }
    }

    /**
     * @Route("/survey/delete/answered/survey/{answerId}", name="atm_survey_delete_answered_survey")
     */
    public function deleteAnsweredSurveyAction($answerId){
        $em = $this->getDoctrine()->getManager();
        $answer = $em->getRepository('ATMSurveyBundle:Answer')->findOneById($answerId);
        $survey = $answer->getSurvey();

        $em->remove($answer);
        $em->flush();

        return $this->redirect($this->get('router')->generate('atm_survey_see_answers',array('surveyId'=>$survey->getId())));
    }

    /**
     * @Route("/survey/search/users/{searchString}/{surveyId}", name="atm_survey_search_users", options={"expose"=true})
     */
    public function searchUsersAction($searchString,$surveyId){
        $em = $this->getDoctrine()->getManager();

        $configuration = $this->getParameter('atm_survey_config');
        $qb = $em->createQueryBuilder();
        $qb
            ->select('partial u.{id,username,usernameCanonical,email}')
            ->from($configuration['user'],'u')
            ->where(
                $qb->expr()->orX(
                    $qb->expr()->like('u.username',$qb->expr()->literal('%'.$searchString.'%')),
                    $qb->expr()->like('u.email',$qb->expr()->literal('%'.$searchString.'%'))
                )
            );

        $users = $qb->getQuery()->getArrayResult();

        return $this->render('ATMSurveyBundle:Admin:Survey/searchUsers.html.twig',array(
            'users' => $users,
            'surveyId'=> $surveyId
        ));
    }

    /**
     * @Route("/survey/name/edit/{surveyId}", name="atm_survey_edit_name", options={"expose"=true})
     */
    public function editSurveyNameAction($surveyId){
        $em = $this->getDoctrine()->getManager();

        $request = $this->get('request_stack')->getCurrentRequest();
        $name = $request->get('name');

        if(!empty($name)){
            $survey = $em->getRepository('ATMSurveyBundle:Survey')->findOneById($surveyId);
            $survey->setName($name);
            $em->persist($survey);
            $em->flush();
        }

        return $this->redirect($this->get('router')->generate('atm_survey_list'));
    }

    /**
     * @Route("/list/interactive/questions/{modelId}", name="atm_survey_list_interactive_questions", defaults={"modelId":null}, options={"expose"=true})
     */
    public function interactiveQuestionsAction($modelId){
        $configuration = $this->getParameter('atm_survey_config');
        $em = $this->getDoctrine()->getManager();

        $qbQuestions = $em->createQueryBuilder();
        $qbQuestions
            ->select('partial iq.{id,question,creation_date}')
            ->addSelect('partial a.{id,username,title}')
            ->addSelect('partial m.{id,username,title}')
            ->from('ATMSurveyBundle:InteractiveQuestion','iq')
            ->join('iq.author','a')
            ->where($qbQuestions->expr()->eq('iq.question_reviewed',0))
            ->orderBy('iq.creation_date','DESC');

        $models = null;
        if(is_null($modelId)){
            $qbQuestions->join('iq.model','m');
            $questions = $qbQuestions->getQuery()->getArrayResult();
            $modelsInQuestions = array();
            foreach($questions as $question){
                if(!in_array($question['model']['id'],$modelsInQuestions)){
                    $modelsInQuestions[] = $question['model']['id'];
                }
            }
            
            if(count($modelsInQuestions)> 0){
                $qbModels = $em->createQueryBuilder();
                $qbModels
                    ->select('partial m.{id,username}')
                    ->from($configuration['user'],'m')
                    ->where(
                        $qbModels->expr()->in('m.id',$modelsInQuestions)
                    )
                    ->orderBy('m.username','ASC');

                $models = $qbModels->getQuery()->getArrayResult();
            }

        }else{
            $qbQuestions->join('iq.model','m','WITH',$qbQuestions->expr()->eq('m.id',$modelId));
        }



        return $this->render('ATMSurveyBundle:Admin:InteractiveQuestions/interactive_questions.html.twig',array(
            'models' => $models,
            'questions' => $qbQuestions->getQuery()->getArrayResult()
        ));
    }

    /**
     * @Route("/list/answered/interactive/questions}", name="atm_survey_list_answered_interactive_questions", options={"expose"=true})
     */
    public function answeredInteractiveQuestionsAction(){
        $em = $this->getDoctrine()->getManager();

        $qbQuestions = $em->createQueryBuilder();
        $qbQuestions
            ->select('partial iq.{id,question,creation_date,answer}')
            ->addSelect('partial a.{id,username,title}')
            ->addSelect('partial m.{id,username,title}')
            ->from('ATMSurveyBundle:InteractiveQuestion','iq')
            ->join('iq.author','a')
            ->join('iq.model','m')
            ->where(
                $qbQuestions->expr()->andX(
                    $qbQuestions->expr()->eq('iq.question_reviewed',1),
                    $qbQuestions->expr()->eq('iq.answer_reviewed',0),
                    $qbQuestions->expr()->isNotNull('iq.answer')
                )
            )
            ->orderBy('iq.creation_date','DESC');

        return $this->render('ATMSurveyBundle:Admin:InteractiveQuestions/answered_interactive_questions.html.twig',array(
            'questions' => $qbQuestions->getQuery()->getArrayResult()
        ));
    }

    /**
     * @Route("/delete/interactive/question/{interactiveQuestionId}", name="atm_survey_delete_interactive_question")
     */
    public function deleteInteractiveQuestionAction($interactiveQuestionId){
        $em = $this->getDoctrine()->getManager();

        $interactiveQuestion = $em->getRepository('ATMSurveyBundle:InteractiveQuestion')->findOneById($interactiveQuestionId);
        $em->remove($interactiveQuestion);
        $em->flush();

        return $this->redirect($this->get('router')->generate('atm_survey_list_interactive_questions'));
    }

    /**
     * @Route("/interactive/questions/approve/{questionId}", name="atm_survey_interactive_questions_approve")
     */
    public function approveInteractiveQuestion($questionId){
        $em = $this->getDoctrine()->getManager();

        $interactiveQuestion = $em->getRepository('ATMSurveyBundle:InteractiveQuestion')->findOneById($questionId);

        $interactiveQuestion->setQuestionReviewed(true);
        $em->persist($interactiveQuestion);
        $em->flush();

        $event = new InteractiveQuestionReviewed($interactiveQuestion);
        $this->get('event_dispatcher')->dispatch(InteractiveQuestionReviewed::NAME, $event);

        return $this->redirect($this->get('router')->generate('atm_survey_list_interactive_questions'));
    }

    /**
     * @Route("/interactive/questions/form/survey", name="atm_survey_interactive_questions_show_survey_form")
     */
    public function showSurveyFormInteractiveQuestionsAction(){
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request_stack')->getCurrentRequest();

        $interactiveQuestionsIds = json_decode($request->get('questionIds'));
        $interactiveQuestions = array();
        foreach($interactiveQuestionsIds as $iq){
            $interactiveQuestions[] = $em->getRepository('ATMSurveyBundle:InteractiveQuestion')->findOneById($iq);
        }

        return $this->render('ATMSurveyBundle:Admin:InteractiveQuestions/createSurvey.html.twig',array(
            'interactiveQuestions' => $interactiveQuestions
        ));
    }

    /**
     * @Route("/interactive/questions/create/survey", name="atm_survey_interactive_questions_create_survey")
     */
    public function createFromInteractiveQuestionsAction(){
        $em = $this->getDoctrine()->getManager();
        $config = $this->getParameter('atm_survey_config');
        $request = $this->get('request_stack')->getCurrentRequest();

        $surveyName = $request->get('survey_name');
        $survey = new Survey();
        $survey->setName($surveyName);

        $web_folder = $this->get('kernel')->getRootDir().'/../web/';
        $mediaDir = $web_folder.$config['media_folder'];
        if(!is_dir($mediaDir)){
            mkdir($mediaDir,0777);
        }

        $headerImage = $request->files->get('flHeaderImage');
        if(!is_null($headerImage)){
            $extension = $headerImage->guessExtension();
            $filename = md5(uniqid()).'.'.$extension;
            $headerImage->move($mediaDir,$filename);
            $survey->setHeaderImage($config['media_folder'].'/'.$filename);
            $em->persist($survey);
        }

        $headerImageMobile = $request->files->get('flHeaderImageMobile');
        if(!is_null($headerImageMobile)){
            $filenameMobile = md5(uniqid()).'.'.$headerImageMobile->guessExtension();
            $headerImageMobile->move($mediaDir,$filenameMobile);
            $survey->setHeaderImageMobile($config['media_folder'].'/'.$filenameMobile);
            $em->persist($survey);
        }

        $headerImageMiddle = $request->files->get('flHeaderImageMiddle');
        if(!is_null($headerImageMiddle)){
            $filenameMiddle = md5(uniqid()).'.'.$headerImageMiddle->guessExtension();
            $headerImageMiddle->move($mediaDir,$filenameMiddle);
            $survey->setHeaderImageMiddle($config['media_folder'].'/'.$filenameMiddle);
            $em->persist($survey);
        }

        $profileImage = $request->files->get('flProfileImage');
        if(!is_null($profileImage)){
            $extension = $profileImage->guessExtension();
            $filename = md5(uniqid()).'.'.$extension;
            $profileImage->move($mediaDir,$filename);
            $survey->setProfileImage($config['media_folder'].'/'.$filename);
            $em->persist($survey);
        }

        $initDate = $request->get('initDate');
        if(!is_null($initDate)){
            $initDate = DateTime::createFromFormat('d-m-Y',$initDate);
            $survey->setInitDate($initDate);
            $em->persist($survey);
        }

        $endDate = $request->get('endDate');
        if(!is_null($endDate)){
            $endDate = DateTime::createFromFormat('d-m-Y',$endDate);
            $survey->setEndDate($endDate);
            $em->persist($survey);
        }

        $questions = $request->get('question');
        foreach($questions as $interactiveQuestionId => $question){
            $interactiveQuestion = $em->getRepository('ATMSurveyBundle:InteractiveQuestion')->findOneById($interactiveQuestionId);
            $author =  $interactiveQuestion->getAuthor();
            $em->remove($interactiveQuestion);

            $surveyQuestion = new Question();
            $surveyQuestion->setQuestion($question);
            $surveyQuestion->setSurvey($survey);
            $surveyQuestion->setType(Question::type_text);
            $surveyQuestion->setAskedBy($author);
            $em->persist($surveyQuestion);
        }

        $em->flush();

        return $this->redirect($this->get('router')->generate('atm_survey_list_interactive_questions'));
    }


    /**
     * @Route("/interactive/questions/approve/answer/{questionId}", name="atm_survey_interactive_questions_approve_answer")
     */
    public function approveInteractiveQuestionAnswerAction($questionId){

        $em = $this->getDoctrine()->getManager();

        $interactiveQuestion = $em->getRepository('ATMSurveyBundle:InteractiveQuestion')->findOneById($questionId);

        $interactiveQuestion->setAnswerReviewed(true);
        $em->persist($interactiveQuestion);
        $em->flush();

        $event = new InteractiveQuestionAnswerApproved($interactiveQuestion);
        $this->get('event_dispatcher')->dispatch(InteractiveQuestionAnswerApproved::NAME, $event);

        return $this->redirect($this->get('router')->generate('atm_survey_list_answered_interactive_questions'));
    }

    /**
     * @Route("/models/allow/interactive/questions", name="atm_survey_allow_interactive_questions")
     */
    public function modelsAllowInteractiveQuestionsAction(){
        $config = $this->getParameter('atm_survey_config');
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();

        $qb
            ->select('partial u.{id,username}')
            ->from($config['user'],'u')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('u.approved',1),
                    $qb->expr()->eq('u.enabled',1),
                    $qb->expr()->eq('u.locked',0),
                    $qb->expr()->like('u.roles',$qb->expr()->literal('%ROLE_MODEL%'))
                )
            )
            ->orderBy('u.username','ASC');

        return $this->render('ATMSurveyBundle:Admin:InteractiveQuestions/models_allow.html.twig',array(
            'models' => $qb->getQuery()->getArrayResult()
        ));

    }

    /**
     * @Route("/allow/questions/model/{userId}/{allowed}", name="atm_survey_allow_model", options={"expose"=true})
     */
    public function allowQuestionsAction($userId,$allowed){
        $config = $this->getParameter('atm_survey_config');
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();

        $qb
            ->select('aq')
            ->from('ATMSurveyBundle:AllowQuestions','aq')
            ->join('aq.user','u','WITH',$qb->expr()->eq('u.id',$userId));

        $allowQuestions = $qb->getQuery()->getResult();

        if($allowQuestions){
            $allowQuestions = $allowQuestions[0];
            $allowQuestions->setAllowQuestions($allowed == 1 ? true : false);
        }else{
            $model = $em->getRepository($config['user'])->findOneById($userId);

            $allowQuestions = new AllowQuestions();
            $allowQuestions->setUser($model);
            $allowQuestions->setAllowQuestions(false);
        }

        $em->persist($allowQuestions);
        $em->flush();

        return new Response('ok');

    }
}
