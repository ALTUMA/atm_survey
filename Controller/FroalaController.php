<?php

namespace ATM\SurveyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class FroalaController extends Controller{
    /**
     * @Route("/upload/image/{folder}", name="atm_survey_froala_upload_image", options={"expose"=true})
     */
    public function uploadImageAction($folder){
        $config = $this->getParameter('atm_survey_config');
        $request = $this->get('request_stack')->getCurrentRequest();

        $surveyImagesFolder = $this->get('kernel')->getRootDir().'/../web/media/'.$config['froala']['media_folder'];
        if(!is_dir($surveyImagesFolder)){
            mkdir($surveyImagesFolder);
        }

        $messageImagesFolder = $surveyImagesFolder.'/'.$folder;
        if(!is_dir($messageImagesFolder)){
            mkdir($messageImagesFolder);
        }

        $file = $request->files->get('file');
        $imageName = md5(uniqid()).'.'.$file->guessExtension();
        $file->move($messageImagesFolder,$imageName);

        $hwcdn = $this->get('xlabs_hwcdn');
        return new Response(json_encode(array('link'=>$hwcdn->getCDNResource(array('media_path' =>'/media/'.$config['froala']['media_folder'].'/'.$folder.'/'.$imageName)))));
    }

    /**
     * @Route("/delete/image/{path}", name="atm_survey_froala_delete_image", options={"expose"=true})
     */
    public function deleteImageAction($path){
        $pathDecoded = urldecode($path);
        $pathDecoded = str_replace('https://cdn.static.3tech.xyz','',$pathDecoded);
        $imageFolder = $this->get('kernel')->getRootDir().'/../web'.urldecode($pathDecoded);
        unlink($imageFolder);
        return new Response(json_encode('success'));
    }

}