<?php

namespace ATM\SurveyBundle\Extension;

use Doctrine\ORM\EntityManagerInterface;

class SurveyExtension extends \Twig_Extension
{
    private $em;
    private $config;

    public function __construct(EntityManagerInterface $em,$config)
    {
        $this->em = $em;
        $this->config = $config;
    }


    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('getUserAnswer', array($this, 'getUserAnswer')),
            new \Twig_SimpleFunction('ATMSurveyUserTotalInteractiveQuestions', array($this, 'getUserTotalInteractiveQuestions')),
            new \Twig_SimpleFunction('ATMSurveyModelAllowQuestions', array($this, 'modelAllowQuestions')),
        );
    }

    public function getUserAnswer($question,$answers){

        foreach($answers as $answer){
            if($question->getId() == $answer['id']){
                if($question->getType() == 'text'){
                    return $answer['text'];
                }else{
                    $choices = array();
                    foreach($answer as $key=>$value){
                        if($key != 'id'){
                            $choices[] =  $value;
                        }
                    }
                    return $choices;
                }
            }
        }
    }

    public function getUserTotalInteractiveQuestions($userId,$modelId){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('COUNT(iq.id) as totalQuestions')
            ->from('ATMSurveyBundle:InteractiveQuestion','iq')
            ->join('iq.author','a','WITH',$qb->expr()->eq('a.id',$userId))
            ->join('iq.model','m','WITH',$qb->expr()->eq('m.id',$modelId))
            ->where(
                $qb->expr()->orX(
                    $qb->expr()->neq('iq.question_reviewed',1),
                    $qb->expr()->neq('iq.answer_reviewed',1)
                )
            )
        ;

        $result = $qb->getQuery()->getArrayResult();

        return  $this->config['user_max_interactive_questions'] - $result[0]['totalQuestions'];
    }

    public function modelAllowQuestions($userId){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('aq')
            ->from('ATMSurveyBundle:AllowQuestions','aq')
            ->join('aq.user','u','WITH',$qb->expr()->eq('u.id',$userId));

        $allowQuestions = $qb->getQuery()->getArrayResult();

        return empty($allowQuestions) ? null : $allowQuestions[0];
    }
}