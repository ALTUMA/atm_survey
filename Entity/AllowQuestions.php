<?php

namespace ATM\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="atm_allow_questions")
 */
class AllowQuestions{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    protected $user;

    /**
     * @ORM\Column(name="allow_questions", type="boolean", nullable=false,  options={"default" : 1})
     */
    protected $allow_questions;


    public function getId()
    {
        return $this->id;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getAllowQuestions()
    {
        return $this->allow_questions;
    }

    public function setAllowQuestions($allow_questions)
    {
        $this->allow_questions = $allow_questions;
    }
}