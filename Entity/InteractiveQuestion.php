<?php

namespace ATM\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="atm_interactive_question")
 */
class InteractiveQuestion{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creation_date;

    /**
     * @ORM\Column(name="question", type="text", nullable=false)
     */
    private $question;

    /**
     * @ORM\Column(name="question_reviewed", type="boolean", nullable=false,  options={"default" : 0})
     */
    private $question_reviewed;

    /**
     * @ORM\Column(name="answer", type="text", nullable=true)
     */
    private $answer;

    /**
     * @ORM\Column(name="answer_reviewed", type="boolean", nullable=false,  options={"default" : 0})
     */
    private $answer_reviewed;

    protected $author;

    protected $model;

    public function __construct()
    {
        $this->creation_date = new \DateTime();
        $this->question_reviewed = false;
        $this->answer_reviewed = false;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreationDate()
    {
        return $this->creation_date;
    }

    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }

    public function getQuestion()
    {
        return $this->question;
    }

    public function setQuestion($question)
    {
        $this->question = $question;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        $this->model = $model;
    }

    public function getQuestionReviewed()
    {
        return $this->question_reviewed;
    }

    public function setQuestionReviewed($question_reviewed)
    {
        $this->question_reviewed = $question_reviewed;
    }

    public function getAnswerReviewed()
    {
        return $this->answer_reviewed;
    }

    public function setAnswerReviewed($answer_reviewed)
    {
        $this->answer_reviewed = $answer_reviewed;
    }

    public function getAnswer()
    {
        return $this->answer;
    }

    public function setAnswer($answer)
    {
        $this->answer = $answer;
    }
}