$('.btnAddQuestion').click(function(e){
    e.preventDefault();
    var question = $('.question_clone').clone();
    question.removeClass('question_clone').addClass('question');

    var index = $('.question').length;
    question.attr('id','question_' + index);

    $('.questions_container').append(question);
    question.show();
});

$('.questions_container').on('change','.slcQuestionType',function(){
    var select = $(this);
    var questionType = select.val();
    var parent = select.parent().parent();
    var choiceContainer = parent.find('.choice_container');

    if(questionType != 'text'){
        parent.find('.typeContainer').show();

        if(parent.find('.choice_container').children().length > 0){
            var options = choiceContainer.find('input');
            var parentId = parent.attr('id');
            options.each(function(index ,element){
                if(questionType == 'single_choice'){
                    $(element).attr('type','radio').attr('name',parentId + '_rd');
                }else{
                    $(element).attr('type','checkbox').attr('name',parentId + '_chk_' + index);
                }
            });
        }
    }else{
        choiceContainer.html('');
        parent.find('.typeContainer').hide();
    }
});

var questionContainer;
$('.questions_container').on('click','.addChoice',function(){
    $('#popAddOption').modal('show');
    $('#txOptionText').val('');
    questionContainer = $(this).parent().parent();
});
$('#popAddOption').on('shown.bs.modal', function () {
    $('#txOptionText').focus();
})

$('#txOptionText').keypress(function(e){
    if(e.keyCode == 13){
        $('#btnSaveOption').click();
    }
});

$('#btnSaveOption').click(function(){
    var optionsContainer = questionContainer.find('.choice_container');
    var questionContainerId = questionContainer.attr('id');
    var text = $('#txOptionText').val();
    var questionType = questionContainer.find('.slcQuestionType').val();
    var input;

    if(questionType == 'single_choice'){
        input = '<input type="radio" name="'+ questionContainerId +'_rd" value="'+text+'">';
    }else{
        var numOptions = optionsContainer.find('input').length;
        input = '<input type="checkbox" name="'+ questionContainerId +'_chk_'+numOptions+'"  value="'+text+'">';
    }
    var option = '<div><label>'+input + ' <span class="choiceText">' + text + '</span></label><span class="removeChoice" onclick="$(this).parent().remove();">X</span></div>';
    optionsContainer.append(option);
    $('#popAddOption').modal('hide');
});