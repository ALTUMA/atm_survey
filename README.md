A Survey Manager System

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require atm/surveybundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new ATM\SurveyBundle\ATMSurveyBundle(),
    ];
}
```

## Routing

Append to main routing file:

``` yml
# app/config/routing.yml
  
atm_survey_admin:
    resource: "@ATMSurveyBundle/Controller/AdminController.php"
    type: annotation
    prefix:   /members/admin

atm_survey:
    resource: "@ATMSurveyBundle/Controller/SurveyController.php"
    type: annotation
    prefix:   /
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml
  
atm_survey:
    user: User namespace
    redirect_route_after_survey: route after survey submision
```