<?php

namespace ATM\SurveyBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        return $treeBuilder->root('atm_survey')
                                ->children()
                                    ->scalarNode('user')->isRequired()->end()
                                    ->scalarNode('redirect_route_after_survey')->isRequired()->end()
                                    ->scalarNode('media_folder')->isRequired()->end()
                                    ->arrayNode('froala')->isRequired()
                                        ->children()
                                            ->scalarNode('key')->isRequired()->end()
                                            ->scalarNode('media_folder')->isRequired()->end()
                                        ->end()
                                    ->end()
                                    ->scalarNode('user_max_interactive_questions')->defaultValue('3')->end()
                                ->end()
                            ->end();
    }
}
